package com.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.model.Sample;

public interface SampleRepository extends JpaRepository<Sample, Long> {

}
